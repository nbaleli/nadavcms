	


//***********************************************************************************************
//
//						configuration variables
//
//***********************************************************************************************


	window.arr = jQuery.extend({}, $.cookie());
	console.log(window.arr);


	
		$(document).ready(function() {
					
//***********************************************************************************************
//
//						active color main + cookies
//
//***********************************************************************************************

			$('.main_nav_item').each(function(){
				var elem = '#' + $(this).attr('id'),
					slice = elem.slice(1),
					show = '#' + $(elem).next('ul').attr('id');
				//cookies remember last item's state (open/close)
				console.log($.cookie(slice)); 
				if($.cookie(slice) == 'open') {
					$(elem).removeClass('main_nav_item');
					$(elem).addClass('main-active');
					arr[slice] = 'open';
					//todo: find out why this is not working...
					// checkStart(elem);
					$(show).slideToggle(250);
				} 

//***********************************************************************************************
//
//						click events for main navigation
//
//***********************************************************************************************
		

				//colors of open/close items
				$(elem).click(function(){
					checkStart(elem);
				});
			});
			

			$('.update_event').click(function(){
				// var current = $(this).attr('id');
				// current = current[current.length - 1];
				var current = $(this).attr('id');
				current = current.substr(12);
				// console.log(current);
				var month = $('#months' + current).val();
				var day = $('#days' + current).val();
				var year = $('#year' + current).val();
				var location = $('#location' + current).val();
				var icon = $('#event_icon' + current).val();
				

			$.ajax({
				url: "actions/update/update_event.php",
				type: "POST",
				cache: false,
				data: "month="+month+"&day="+day+"&year="+year+"&location="+location+"&icon="+"../images/mini icons/"+icon+".png"+"&current="+current,
				dataType: "html",
				beforeSend: function() {
					console.log('sending ajax');
				},
				success: function(my_msg) {
					console.log(my_msg);
				},
				error:  function() {
					console.log('ajax error');
				},
				complete: function() {
					console.log('ajax complete');
					alert('Update Complete!')
					window.location.reload();
				},
				timeout: 3000
			});
			});

			$('#update_all').click(function(){
				$("[class^=update_event]").click();
				location.reload();
			});

			$('.delete_event').click(function(){
				var current = $(this).attr('id');
				current = current[current.length - 1];
				// console.log(current);0
				function confirm_delete(){
				var yes = 
					confirm(
						"                                           Warning!!!" + "\n" + 
						"                        you're about to delete this event," + "\n" + 
						"                                          are you sure?"
					);
				var no;
				
				if (yes==true)
				  {
					$.ajax({
						url: "actions/delete/delete_event.php",
						type: "POST",
						cache: false,
						data: "current="+current,
						dataType: "html",
						beforeSend: function() {
							console.log('sending ajax');
						},
						success: function(my_msg) {
							console.log(my_msg);
						},
						error:  function() {
							console.log('ajax error');
						},
						complete: function() {
							console.log('ajax complete');
							location.reload();
						},
						timeout: 3000
					});
				  }
				 };
				 confirm_delete();
				 });

			$('#add_event').click(function(){
				console.log(window.location);
				var month;
				var day;
				var year;
				var location;
				var icon;
				$.ajax({
					url: "actions/add/add_event.php",
					type: "POST",
					cache: false,
					data: "month="+"Jan"+"&day="+"01"+"&year="+"2013"+"&location="+"location"+"&icon="+"../images/mini icons/CAR.png",
					dataType: "html",
					beforeSend: function() {
						console.log('sending ajax');
					},
					success: function(my_msg) {
						console.log(my_msg);
					},
					error:  function() {
						console.log('ajax error');
					},
					complete: function() {
						console.log('ajax complete');
						window.location.reload();
					},
					timeout: 3000
				});
			});



//***********************************************************************************************
//
//						functions
//
//***********************************************************************************************

			function checkStart(x) {
					var slice = x.slice(1),
						show = '#' + $(x).next('ul').attr('id');
						console.log(arr[slice] + ' is arr[slice]');
					if (arr[slice] == 'open') {
						console.log('I should CLOSE');
						// console.log('its in the object, setting to OFF');				
						$(x).removeClass('main-active');
						$(x).addClass('main_nav_item');
						$.cookie(slice, "");
						// console.log('cookie[slice] is ' + $.cookie(slice));

					} else {
						console.log('I should OPEN');
						$(x).removeClass('main_nav_item');
						$(x).addClass('main-active');
						$.cookie(slice, 'open');
						// console.log('setting to false'); 
						// console.log('cookie[slice] is ' + $.cookie(slice));
					}

					$(show).slideToggle(250);

					arr[slice] = arr[slice] == 'open' ? "" : 'open';
					console.log(arr[slice]);
				}



		});
	