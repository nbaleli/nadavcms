<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>CMS</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Michroma' rel='stylesheet' type='text/css'>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="jquery.cookie.js"></script>
	<script src="js/script.js"></script>
  	<link rel="stylesheet" href="/resources/demos/style.css" />
<?php 
include('db.php');


?>

</head>
<body>
	<div class="header">
		<div class="header_content">
			<h1>CMS</h1>
			<img src="images/cms.png" width="90px" height="76px" />
		</div>
	</div>
	<div class="wrapper">
		<ul class="main_nav">
			<li class="main_nav_item" id="home">Home</li>
				<ul class="home_slide" id="home_content">	
					<li id="carusel">Carusel</li>
				</ul>
			<li class="main_nav_item" id="biography">Biography</li>
			<li class="main_nav_item" id="tourdates">Tour-Dates</li>
				<ul class="tourdates_slide" id="events_content">
					<div id="update_all">Update All</div>
					<div id="add_event">Add Event</div>
					<!-- <li id="events">Events</li> -->
						<!-- <div id="events_container"> -->

							<?php 
							//print_r($events_array);echo '<br/>'.count($events_array);
							foreach ($events_array as $event => $value) { ?>
							  	<div class="event_box">
							  		<h4 style="font-size:16px; color:#00B7FF; background-color: #161616; padding-left:10px; padding-bottom:3px; border: 1px solid #DBDBDB; font-weight:bold;"><?php echo date("M - d - Y", $value['events_timestamp'])." ______ ".$value['events_location']; ?></h4>
							  		<div style="clear:both"></div><br/>
							  		<div class="event_months">
									  	<h6>Month:</h6>
									    <select id="months<?php echo $value['events_id']; ?>">
									    	<?php
												

									    		$the_month = date('M', $value['events_timestamp']) ;
												$the_day = date('d', $value['events_timestamp']) ;
												$the_year = date('Y', $value['events_timestamp']) ;
												$months = array(Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec);

												foreach ($months as $key => $mvalue) {
													$selected = ($the_month == $mvalue) ? 'selected' : '';
												    echo "<option ".$selected.">".$mvalue."</option>";

												}
											?>
										</select>
									
									</div>
									<div class="event_days">
										<h6>Day:</h6>
										<select id="days<?php echo $value['events_id']; ?>">
											<?php
											for ( $i=1;$i<=31;$i++)
											{ 
												//adds a zero for 1-9
												$z = $i < 10 ? 0 : '';
												$selected = ($the_day == $i) ? 'selected' : '';
												echo "<option ".$selected.">".$z.$i."</option>";
											}
											?>
										</select>
									</div>
									<div class="event_year">
										<h6>Year:</h6>
										<input type="text" id="year<?php echo $value['events_id']; ?>"name="event year" maxlength="4" size="3" value="<?php echo $value['events_year']; ?>">
								  	</div>
									<div class="event_location">
										<h6>Location:</h6>
										<input type="text" id="location<?php echo $value['events_id']; ?>"name="event location" maxlength="31" size="33" value="<?php echo $value['events_location']; ?>">
								  	</div>
								  	<div class="event_icon" style="float: left">
								  		<h6>Icon:</h6>
								  		<select id="event_icon<?php echo $value['events_id']; ?>">
									    	<?php
												$icons = array("../images/mini icons/AIR.png","../images/mini icons/CAR.png");
												foreach ($icons as $key => $ivalue) {
													$selected = ($value['events_icon'] == $ivalue) ? 'selected' : '';
												    echo "<option ".$selected.">".substr($ivalue, 21, 3)."</option>";
												}
											?>
										</select>
									</div>
									<div id="delete_event<?php echo $value['events_id']; ?>" class="delete_event">Delete</div>
									<div id="update_event<?php echo $value['events_id']; ?>" class="update_event">Update</div>
								</div>
						  <?php } ?>
						<!-- </div> -->
				</ul>
			<li class="main_nav_item" id="gallery">Gallery</li>
			<li class="main_nav_item" id="contact">Contact</li>
		</ul>

	
</body>
</html>